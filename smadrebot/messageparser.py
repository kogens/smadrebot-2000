import re
import os
import logging
from discord import Forbidden, HTTPException
from textgenrnn import textgenrnn

""" This module parses any messages and determine if it was relevant for the bot and what to respond.
 The actual generation of e.g. jingles is done elsewhere but called from here. """

class MessageParser:
    def __init__(self, db_handler, prefix='!', temperature=0.8):
        logging.info('Preparing message parser, textgen temperature: ' + str(temperature))
        self.db_handler = db_handler
        self.prefix = prefix
        self.message_regex = {'jingle': prefix + '\w*j\w*g\w*l\w*',
                             'botmention': 'm+i*l+e*y+',
                             'goodbot': prefix + 'goodbot',
                             'archive': prefix + r'(arkiv|old) *(\d*)',
                             'answer': prefix + 'svar'}

        self.datafolder = os.path.join(os.path.dirname(__file__), 'data')
        # TODO handle if model is missing
        weights = os.path.join(self.datafolder, 'model_weights.hdf5')
        vocab = os.path.join(self.datafolder, 'model_vocab.json')
        config = os.path.join(self.datafolder, 'model_config.json')

        try:
            self.generator = textgenrnn(weights_path=weights, vocab_path=vocab, config_path=config)
        except (FileNotFoundError, OSError) as e:
            logging.error('Cannot find model for text generation!')
            logging.error(e)
            raise e

        self.temperature = temperature

    def parse(self, message):
        for message_type, regex in self.message_regex.items():
            results = re.search(regex, message.content.lower())
            if results is not None:
                logging.debug('Matched ' + message_type)
                return message_type

    async def respond_to_message(self, message):
        message_type = self.parse(message)
        if message_type == 'jingle':
            logging.info('Generating funny jingle')

            async with message.channel.typing():
                jingle = self.generate_jingle()
            logging.info(jingle)

            content = jingle

        elif message_type == 'answer':
            logging.info('Generating answer')

            async with message.channel.typing():
                content = self.generate_jingle(prefix='')
            logging.info(content)

        elif message_type == 'archive':
            logging.info('Responding with archive message')

            async with message.channel.typing():
                matches = re.search(self.message_regex[message_type], message.content.lower())
                try:
                    amount = int(matches.group(2))
                except ValueError:
                    amount = 1
                content = self.get_archived(amount)

        elif message_type == 'goodbot':
            logging.info('Reacting nicely to goodbot feedback')
            await message.add_reaction('❤')
            #time.sleep(0.5)

            content = 'Arj, du er sød {0.author.mention}!'.format(message)

        elif message_type == 'botmention':
            logging.info('Someone said my name! Responding appropriately')
            content = 'Øv {0.author.mention}!'.format(message)
        else:
            content = None

        if content is not None:
            try:
                await message.channel.send(content)
            except HTTPException as e:
                logging.warning(e)


    def get_archived(self, amount=1):
        jingles = []
        if amount <= 10:
            for i in range(amount):
                jingle = self.db_handler.get_archived_jingle()
                if jingle is None:
                    return None
                else:
                    jingles.append(jingle['clean_content'])
            content = '\n'.join(jingles)
        else:
            content = 'Styr dig lige!'
        return content

    def generate_jingle(self, temperature=None, prefix='Smadreland - '):
        if temperature is None:
            temperature = self.temperature
        jingle = self.generator.generate(n=1, temperature=temperature, return_as_list=True)[0]
        jingle = prefix + jingle

        return jingle
