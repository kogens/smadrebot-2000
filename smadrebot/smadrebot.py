import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument('-t', '--test', help='Run in test mode (uses token for test-bot)', action='store_true')
argparser.add_argument('-p', '--production', help='Run in production mode (uses token for real bot)',
                       action='store_true')
argparser.add_argument('-c', '--create', help='Create all tables', action='store_true')
argparser.add_argument('-s', '--scrape', help='Scrape messages from guild into local database', action='store_true')
argparser.add_argument('-I', '--initialize', help='Create database with tables and scrape immediately',
                       action='store_true')
argparser.add_argument('-T', '--temperature', nargs=1, default=0.7, type=float, help='Set text generator temperature')
args = argparser.parse_args()

import os
import logging
import discord

import messageparser
import datahandler

# Setup logging to file
# https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
logfile = os.path.join(os.path.dirname(__file__), 'smadrebot.log')
logging.basicConfig(filename=logfile,
                    level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s: %(name)s: %(message)s')

client = discord.Client()
db_handler = datahandler.DatabaseHandler()
scraper = datahandler.Scraper(client, db_handler)

if args.create:
    db_handler.create_tables()

jingle_temperature = args.temperature[0]
messageparser = messageparser.MessageParser(db_handler, temperature=jingle_temperature)


# Tokens should be environment variables to avoid leaking. Find under "bot" information here:
# https://discord.com/developers/applications/
try:
    if args.test:
        token = os.environ['TOKEN_SMADREBOT_TEST']
        logging.info('Running in test mode')
    else:
        token = os.environ['TOKEN_SMADREBOT']
        logging.info('Starting bot in production')
except KeyError as e:
    logging.error(e)
    raise KeyError('Bot token not found in environment, closing.')


# We're dealing with one specific channel when training the bot, put ID in env. vars
try:
    channel_id = int(os.environ['JINGLE_CHANNEL'])
    logging.info('Jingle channel id: ' + str(channel_id))
except KeyError as e:
    logging.warning(e)
    raise KeyError('No channel for jingles found, cannot scrape.')

@client.event
async def on_ready():
    logging.info('Logged in as {0.user}'.format(client))
    if args.scrape:
        await scraper.scrape_channel(channel_id)
    elif args.initialize:
        db_handler.create_tables()
        await scraper.scrape_channel(channel_id)

    await scraper.daily_action_on_channel(channel_id, action_time='04:00')

@client.event
async def on_message(message):
    # Ignore if the message is from the bot
    if message.author == client.user:
        return
    await messageparser.respond_to_message(message)


client.run(token)