import re
import sqlite3
from datetime import datetime, timedelta
import logging
from discord import Forbidden, HTTPException
from asyncio import sleep
import os
from pathlib import Path

""" Here we deal with all things related to storing and retrieving 
data from the database. Functionality for scraping a Discord server is also here."""

class DatabaseHandler:
    def __init__(self, db_file='messagedata.db'):
        logging.info('Creating database handler for ' + db_file)
        self.datafolder = os.path.join(os.path.dirname(__file__), 'data')
        self.db_file = os.path.join(self.datafolder, db_file)
        self.conn = None
        self.date_format = '%Y-%m-%d %H:%M:%S'
        self.connect_to_database()

    def connect_to_database(self, row_factory=sqlite3.Row):
        logging.info('Connecting to database ' + self.db_file)
        try:
            # https://stackoverflow.com/questions/811548/sqlite-and-python-return-a-dictionary-using-fetchone
            self.conn = sqlite3.connect(self.db_file)
            self.conn.row_factory = row_factory
        except sqlite3.Error as e:
            logging.error(e)

    def insert_sql(self, sql_statement):
        logging.info('Inserting sql')
        logging.info(sql_statement)

        try:
            c = self.conn.cursor()
            c.execute(sql_statement)
        except sqlite3.Error as e:
            logging.error(e)

    def create_tables(self):
        sql_create_message_table = '''CREATE TABLE IF NOT EXISTS message (
                                                    id integer PRIMARY KEY,
                                                    timestamp date,
                                                    author_name text,
                                                    author_id integer,
                                                    content text,
                                                    clean_content text,
                                                    channel_id integer,
                                                    channel_name text,
                                                    server_id text,
                                                    server_name text,
                                                    scrape_time date,
                                                    message_blob blob);
                                                    '''
        # Extract jingles from messages
        sql_create_jingle_view = '''CREATE VIEW IF NOT EXISTS discord_jingles AS
                                    SELECT * FROM message
                                    WHERE clean_content LIKE '%land%-%' 
                                    AND clean_content NOT LIKE '%-'
                                    AND channel_id LIKE '329226428241674242' 
                                    AND author_id NOT LIKE '422445467465416704'
                                '''

        self.insert_sql(sql_create_message_table)
        self.insert_sql(sql_create_jingle_view)
        return

    def insert_message(self, message):
        time_message = message.created_at.strftime(self.date_format)
        time_now = datetime.now().strftime(self.date_format)

        # https://stackoverflow.com/questions/9336270/using-a-python-dict-for-a-sql-insert-statement

        # Inserting datastructures as binary blob:
        # https://stackoverflow.com/questions/198692/can-i-pickle-a-python-dictionary-into-a-sqlite3-text-field
        # binary_message = sqlite3.Binary(pickle.dumps(message))
        try :
            message_dict = {'id': message.id,
                            'timestamp': time_message,
                            'author_name': message.author.display_name,
                            'author_id': message.author.id,
                            'content': message.content,
                            'clean_content': message.clean_content,
                            'channel_id': message.channel.id,
                            'channel_name': message.channel.name,
                            'server_id': message.server.id,
                            'server_name': message.server.name,
                            'scrape_time': time_now}
        except AttributeError as e:
            message_dict = {'id': message.id,
                            'timestamp': time_message,
                            'author_name': message.author.display_name,
                            'author_id': message.author.id,
                            'content': message.content,
                            'clean_content': message.clean_content,
                            'channel_id': message.channel.id,
                            'channel_name': '',
                            'server_name': '',
                            'server_id': '',
                            'scrape_time': time_now}

        columns = ', '.join(message_dict.keys())
        placeholder = ', '.join(['?'] * len(message_dict))
        sql_insert_message = 'INSERT INTO message ({columns}) values ({values});'.format(columns=columns, values=placeholder)

        logging.debug('Inserting message into db')
        cur = self.conn.cursor()
        try:
            cur.execute(sql_insert_message, tuple(message_dict.values()) )
        except sqlite3.IntegrityError as e:
            logging.info('Message constraint failed, likely because of duplicate')
        except (sqlite3.OperationalError, sqlite3.ProgrammingError) as e:
            logging.error(e)

    def commit(self):
        logging.info('Committing to database')
        self.conn.commit()

    def close_connection(self):
        self.commit()
        logging.info('Closing connection to database')
        self.conn.close()

    def latest_message_on_channel(self, channel_id):
        logging.info('Retrieving latest message on channel')
        sql_latest_on_channel = '''SELECT * FROM message
                                   WHERE channel_id = ? 
                                   ORDER BY timestamp DESC
                                   LIMIT 1'''
        cur = self.conn.cursor()
        cur.execute(sql_latest_on_channel, (channel_id,))
        latest_message = cur.fetchone()

        if latest_message is not None:
            latest_message = dict(latest_message)
            logging.info('Latest message retrieved from channel ' + str(channel_id) + ':')
            logging.info(str(latest_message))
        else:
            logging.info('No messages found in database for channel ' + str(channel_id))

        return latest_message

    def latest_date_on_channel(self, channel_id):
        latest_message = self.latest_message_on_channel(channel_id)
        if latest_message is not None:
            timestamp = latest_message['timestamp']
            latest_date = datetime.strptime(timestamp, self.date_format)
        else:
            latest_date = None

        return latest_date

    def get_archived_jingle(self):
        sql_random_jingle = '''SELECT * FROM discord_jingles
                                WHERE clean_content LIKE '%land%-%' 
                                AND clean_content NOT LIKE '%-'
                                ORDER BY RANDOM() LIMIT 1'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql_random_jingle)
        except sqlite3.OperationalError as e:
            logging.warning(e)
            raise e
        try:
            archived_jingle = dict(cur.fetchone())
            logging.info('Fetched random jingle')
            logging.debug(archived_jingle)
            return archived_jingle
        except TypeError as e:
            logging.error('Jingle table is empty, cannot return archived jingle')
            return None



    def get_all_jingles(self, remove_prefix=False, clean_mojis=True, write_to_file=False, filename='archived_jingles.txt'):
        logging.info('Fetching all jingles from database')
        sql_all_jingles = '''SELECT clean_content FROM discord_jingles
                             WHERE clean_content LIKE '%land%-%'
                             AND clean_content NOT LIKE '%-'
                           '''
        cur = self.conn.cursor()
        cur.row_factory = lambda cursor, row: row[0]
        cur.execute(sql_all_jingles)
        archived_jingles = list(cur.fetchall())

        if remove_prefix:
            pattern = re.compile(r'.*[lL][aA][nN][dD]\w*\s*-\s*(.+)')
            archived_jingles = [re.sub(pattern, r'\g<1>', jingle).replace('\n', ' ').strip() for jingle in archived_jingles]
            archived_jingles = [re.sub(r'<(:\w+:)\d+>', r'\g<1>', jingle) for jingle in archived_jingles]

        if write_to_file:
            datafile = os.path.join(self.datafolder, filename)
            logging.info('Writing all jingles to ' + datafile)
            try:
                os.makedirs('data', exist_ok=True)
                with open(datafile, 'w') as f:
                    f.write('\n'.join(archived_jingles))
            except IOError as e:
                logging.error(e)
                logging.error('Could not write jingles to file')
                raise e

        return archived_jingles


class Scraper:
    def __init__(self, client, db_handler):
        self.db_handler = db_handler
        self.datafolder = os.path.join(os.path.dirname(__file__), 'data')
        self.client = client
        self.date_format = '%Y-%m-%d %H:%M:%S'

    async def scrape_channel(self, channel_id):
        # Kinda slow, as each message is translated into the database format individually.
        # Solution: Get a bunch of messages as a list and treat them in batches. Later...
        # Message history
        # https://discordpy.readthedocs.io/en/stable/api.html#discord.abc.Messageable.history
        # TODO scraper seems to attempt each message twice in logs?
        channel = self.client.get_channel(channel_id)
        if channel is None:
            logging.warning('No channel matching id ' + str(channel_id) + ', scraping cancelled.')
            return
        else:
            logging.info('Scraping channel ' + channel.name + ', id ' + str(channel.id))
            latest_date = self.db_handler.latest_date_on_channel(channel_id)
            if latest_date is None:
                old_date = '2015-01-01 00:00:00'
                latest_date = datetime.strptime(old_date, self.date_format)
                logging.warning('No messages found from channel, scraping ALL after ' + old_date)

            try:
                async for message in channel.history(after=latest_date, limit=500, oldest_first=True):
                    self.db_handler.insert_message(message)

            except AttributeError as e:
                logging.error(e)
            except (Forbidden, HTTPException) as e:
                logging.error(e)
                logging.error('Scraping stopped')
                return

            # Check if there are any newer messages than the latest one, repeat scraping in that case.
            self.db_handler.commit()
            new_latest_date = self.db_handler.latest_date_on_channel(channel_id)
            if new_latest_date != latest_date:
                await self.scrape_channel(channel_id)
        return

    async def daily_action_on_channel(self, channel_id, action_time='04:00', scrape=True, train=True):
        logging.info('Daily scraping set at ' + action_time)
        action_time = datetime.strptime(action_time, '%H:%M')
        while not self.client.is_closed():
            time_now = datetime.now()

            scrapefile = os.path.join(os.path.dirname(__file__), 'SCRAPED')
            try:
                with open(scrapefile, 'r') as f:
                    old_date_string = f.readline()
                    f.close()
                old_date = datetime.strptime(old_date_string, self.date_format)
            except (ValueError, FileNotFoundError) as e:
                logging.warning(e)
                old_date = datetime.now() - timedelta(days=1)
                old_date_string = datetime.strftime(old_date, self.date_format)
                logging.warning('Setting previous scrape time to one day ago, ' + old_date_string)
                with open(scrapefile, 'w') as f:
                    f.write(old_date_string)
                    f.close()


            if (time_now - timedelta(hours=1) > old_date) and (time_now.hour == action_time.hour):
                # Should scrape when the hour is within the desired scrape hour and its been more than one day
                if scrape:
                    try:
                        await self.scrape_channel(channel_id)
                        with open(scrapefile, 'w') as f:
                            f.write(datetime.strftime(time_now, self.date_format))
                            f.close()
                    except sqlite3.OperationalError as e:
                        logging.error('Scraping unsuccessful')
                        logging.error(e)

            #if train:
            #   #await self.train_on_all()
            #    continue

            await sleep(10)
