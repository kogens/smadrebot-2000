# Smadrebot
A personal [Discord chatbot](https://discord.com/developers/docs/intro) for a group of friends written in Python. It works by scraping messages into a local [sqlite](https://www.sqlite.org/i) database and then using the [textgenrnn](https://github.com/minimaxir/textgenrnn) python package to train a neural net on these.  Interacting with the bot then results in inappropiate responses. The bot is currently hosted at [PythonAnywhere](https://www.pythonanywhere.com/).

**Note that the bot is hardcoded to work with a specific Discord channel and message format and as such has limited use for anyone outside of that server**.


### Setup
Clone this repository

```sh
git clone https://gitlab.com/kogens/smadrebot-2000.git
```

Install requirements
```
python3 -m pip install requirements.txt
```

Run the bot with `--initialize` argument to setup the sqlite database with tables and scrape the relevant data.

```sh
python3 smadrebot/smadrebot.py --initialize
```

### Usage
Coming soon™
